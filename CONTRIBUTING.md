# Chronologue contributing guide

Welcome to the Chronologue working group! We’re excited to have you join and share your talents with us. If you would like to contribute, this is a list of contributions we are currently accepting:

* Documentation

    We need help to test templates. We also accept contributions to our [documentation site](https://docs-chronologue.netlify.app/) that helps us explain the Chronologue world.

* Mock tool

    We accept contributions to the front-end development of our mock tool. If you know HTML, CSS, JavaScript, or React, please reach out to us!


However, at this time, we are not accepting the following contributions:

* UX design

To get you started, this guide will give you the basics of our project and how we work.


## Chronologue overview

The Chronologue Project is a [fictional software project](https://gitlab.com/tgdp/chronologue/mock-tool/-/blob/main/README.md) that creates examples of good documentation. To do so, we use The Good Docs Project's (TGDP) templates, meaning that the Chronologue group serves, first and foremost, as a quality tester for the Templates working group.
Our secondary goal is to create an example of a good documentation project in action.

To meet these two goals, our group builds and maintains a fictional product - [The Chronologue](https://docs-chronologue.netlify.app/docs/) - which is a telescope that uses wormhole technology to view and store astronomical events at any point in time. The code for our fictional tool is located at [The Chronologue mock tool](https://gitlab.com/tgdp/chronologue/mock-tool) repository.

This is the repo for our [documentation website](https://docs-chronologue.netlify.app/). It is deployed on [Netlify](https://www.netlify.com/) and is created using the [Hugo](https://gohugo.io/) [Doks Theme](https://getdoks.org/). If you’d like to help us create documentation examples or develop our mock tool, the following sections describe our workflows and how you can start contributing to our project - regardless of your experience level!


## Ground rules

To join our project and start contributing, we ask all new members to attend a [Welcome Wagon](https://www.thegooddocsproject.dev/join-us), which is regularly hosted by TGDP's community managers. At this orientation meeting, you'll get an introduction to our project's goals, key concepts, and workflows.

We expect all members of our project to be nice to each other and to follow our [Code of Conduct](https://thegooddocsproject.dev/code-of-conduct/) when interacting with other members of the Good Docs Project.


## Before you start

Before you start contributing, ensure you have a GitLab account. You can [register for a GitLab account](https://gitlab.com/users/sign_up) if you need to set up a new account. Alternatively, you can also use your GitHub, Google, or other accounts to login into GitLab.

After attending a Welcome Wagon, you can receive access to TGDP's Slack workspace from the community managers. Ask for access to the Chronologue repository by sending your GitLab's username in a message to the `#tech-requests` channel.


### Time commitment

Each member usually spends **1-2 hours per week** on Chronologue-related tasks. We hold weekly meetings dedicated to different types of sessions and topics:

 - Assigning tasks.
 - Planning release cycles.
 - World-building (sessions dedicated to building the fictional Chronologue world, product ecosystem, and other aspects of the project).
 - Co-working (session dedicated to working on our individual tasks).
 - Writing workshop (session dedicated to discuss ideas, roadblocks, ask questions, and more).
 - Running retrospective meetings.

TGDP is a volunteering group, and we understand that members have other commitments, so you are welcome to spend as much time on it as you decide. While we love our work, no one should feel burned out. If something prevents you from completing a project, please let your working group leader know that you won't be able to continue working.


## Joining the Chronologue working group

To stay up-to-date with news about the project, engage with our team, and share your new ideas, join our `#chronologue-docs` channel.

Other channels you might want to join:

* `#general`: Discuss general topics with TGDP community.
* `#tech-requests`: Ask TGDP's tech team anything IT-related and request access to our GitLab repository.
* `#ask-a-community-manager`: Request access to training materials or talk about any community-related topic.

You are welcome to attend our weekly meetings Team Alpaca (Monday evenings if you are in AMER, Tuesday mornings if you are in APAC) and Team Dolphin (Saturday mornings if you are in AMER, Saturday afternoons if you are in EMEA). Check out the [Community Calendar](https://www.thegooddocsproject.dev/calendar) to see meeting hours in your local time. We suggest you add our meeting schedule to your personal calendar. Follow these steps to add it to your Google calendar:


1. Go to [TGDP's calendar](https://www.thegooddocsproject.dev/calendar).
2. Click on any Chronologue session, which will display a dialogue bubble.
3. Copy to your calendar, this will open a new tab on your browser.
4. In your Google calendar, click on **Does not repeat** to display a dropdown menu.
5. Select *Weekly on Saturday* (or Mondays) from the menu.

You will find the Zoom link in the meeting details.

And if you want to add all TGDP meetings to your calendar, click the **Add** button (+) in the lower right corner of the Community Calendar.


![The Good Docs Project community calendar](TGDP-calendar.png)


<br>


## Overview of the Chronologue documentation process

The Chronologue working group starts every cycle after the release of new templates. This Chronologue phase is the final phase of the [template contributing process](https://gitlab.com/tgdp/templates/-/blob/main/CONTRIBUTING.md) where our working group creates an example of the documentation in action.

After each release cycle, we review the new templates in our kickoff meeting, set goals for the team, deadlines, and assign issues and tasks. Attending this meeting is very important, and we encourage all Chronologue members to participate.

The following table describes what happens during every release cycle.


<table>
<!-- Table heading -->
  <tr>
      <th>Phase</th>
      <th>Description</th>
  </tr>
  <!-- Row 1 -->
  <tr>
      <td>Volunteer to create an example of a template</td>
      <td>
         <ul>
            <li>Consult with your working group leader about available Chronologue projects and select a task to work on</li>
            <li>Assign yourself to the <a href="https://gitlab.com/tgdp/chronologue/docs/-/issues">issue</a> tracking that task.</li>
            <li>Follow our issue management guidelines for tickets that already have assignees.</li>
         </ul>
      </td>
  </tr>
  <!-- Row 2 -->
  <tr>
   <td>Propose a documentation scenario</td>
      <td>
         <ul>
            <li>Since the Chronologue tool and world is fake, you need to come up with a possible documentation scenario that can be used to create an example of your template in action.</li>
            <li>Use the Decision log (Google doc) attached to the issue you're working on to create a proposal for your documentation scenario. Since the Chronologue tool and world is fake, you need to come up with a possible documentation scenario that can be used to create an example of your template in action.</li>
            <li>Submit your proposal to the Chronologue canon editorial team for feedback and review. They will make sure the documentation scenario makes sense for the Chronologue world and that it doesn't impact existing Chronologue projects. After getting their approval, you can move onto the next phase.</li>
         </ul>
      </td>
  </tr>
  <!-- Row 3 -->
  <tr>
   <td>Draft the example</td>
      <td>
         <ul>
            <li>Use the Google doc attached to the issue you're working on to create a Chronologue documentation example for a specific template. </li>
            <li>Keep a <a href="https://developerrelations.com/developer-experience/an-introduction-to-friction-logging"> friction log</a> of the template to give feedback to the Templates team. </li>
            <li>Ask questions or work through potential problems you encounter while preparing your draft in the Chronologue writer's workshop meetings. </li>
         </ul>
      </td>
  </tr>
  <!-- Row 4 -->
  <tr>
      <td>Submit your draft to community review</td>
      <td>
         <ul>
            <li>Schedule a review session with your group lead.</li>
            <li>Share you Google doc link with the rest of the team on the day of the session.</li>
            <li>Improve the draft based on feedback. </li>
         </ul>
      </td>
  </tr>
  <!-- Row 5-->
  <tr>
      <td>Submit your draft for Chronologue canon editorial review</td>
      <td>
         <ul>
            <li>When your draft is in a state where you feel it is ready to get merged in, you can work with your working group lead to request an editorial team review.</li>
            <li>The Chronologue canon editorial team is composed of experienced members of the project who reviews your Chronologue example to ensure that it:
                <ul>
                    <li>Makes sense in the Chronologue canon and world.</li>
                    <li>Follows best practices for technical writing.</li>
                    <li>Is consistent with our style guide.</li>
                </ul>
            </li>
            <li>Improve the draft based on feedback. </li>
         </ul>
      </td>
  </tr>
  <!-- Row 6 -->
  <tr>
      <td>Open a merge request</td>
      <td>
         <ul>
            <li>For this phase, you’ll convert drafts from Google Docs or your preferred tool to Markdown and open a merge request in our Chronologue repository. In this phase, it will get reviewed by the working group leads.</li>
            <li>If you are unfamiliar with Git and GitLab, your template working group lead can recommend you for our Git Training workshop.</li>
            <li>Revise documents based on requests from merge request reviewers.</li>
         </ul>
      </td>
  </tr>
  <!-- Row 7 -->
  <tr>
      <td>Give feedback to the templates team (if applicable)</td>
      <td>
         <ul>
            <li>Create an issue in the <a href="https://gitlab.com/tgdp/templates/-/issues">templates repository</a>.</li>
            <li>Give constructive feedback with actionable items.</li>
            <li>Optional: work on the issue, tag a <a href="https://thegooddocsproject.dev/who-we-are/#working-group-leads">templates team leader</a>, and submit a merge request with your changes to the template.</li>
            <li>Send a message in the #templates channel with a link to the issue to notify the team about your feedback.</li>
         </ul>
      </td>
  </tr>
</table>


We are confident that the template creation process ensures a minimum of usability quality. However, if you feel that the template is very deficient to the point that it needs a complete rework, we advise you to follow these steps:

1. Communicate it to your group leader.
2. Open an issue in the templates repository.
3. Describe in detail why you think the template needs a complete rework.
4. Suggest actionable changes.

If you fix the template yourself, tag a [lead of the Templates team](https://thegooddocsproject.dev/who-we-are/#working-group-leads) in your merge request. Otherwise, send the link to the issue in the #templates channels.



## Contribution workflow

We strive for objectivity during the process of testing templates. Our goal when testing templates is to simulate as much as possible technical communicators who are not part of TGDP community and are using TGDP's templates to build their documentation. For this reason, we have no communication with template authors during the testing stage.

Members can collaborate with other TGDP's teams, including the Templates team. However, if you participate in any way during the production of a template, we ask you to skip testing that template. By taking ourselves out of the template production process, we expect to provide the objective quality testing we strive for.



### Issue management

Our [issues board](https://gitlab.com/tgdp/chronologue/docs/-/issues) contains a list of available issues for the Chronologue project. Use our [Chronologue documentation board](https://gitlab.com/tgdp/chronologue/docs/-/boards/5011409) to see our progress and identify priorities. Tickets with the label **Claim me** are waiting to be claimed by volunteers. You can claim an issue, provided it has no assignee(s). Assign yourself to that issue and let the working group leader know about it.

TGDP respects the order in which people claim issues. If you are interested in an issue that has assignee(s), contact the team leader to discuss the possibility of adding you to the issue. We prefer teams with no more than three participants working on the same ticket.

Our members have different levels of experience in technical writing. We find that having a writing colleague to share the journey helps in navigating the writing process and contributing to open-source. Therefore, we work in teams of two to three writers per example document. But we will respect if you wish to work on a project alone. Just let your working group leads know.

To contribute to the mock tool, contact the working group leader who will introduce you to the tech team coordinator.


### Deliverables

To keep ownership and manage an archive of the drafts, The Good Docs Project has pre-generated Google doc files for you to test templates and document your process. Each issue has attached to it three Google doc links which you should use to write and document your progress:

- A **Chronologue example file**: use this file to draft your example document.
- A **decision log**: use this file to propose a possible documentation scenario that can be used to create an example of your template in action. You'll use the decision log to write a proposal for your scenario and then send it to the Chronologue canon editorial team to get feedback on your proposal. They will make sure the documentation scenario makes sense for the Chronologue world and that it doesn’t impact existing Chronologue projects. After getting their approval, you can move onto the next stage.
- A **friction log**: use this file to document the friction points you find when using the template. You can upload your friction log when creating an issue in the Templates repository to give feedback. To learn more about friction logs, we recommend [this article](http://www.trychameleon.com/blog/friction-logs).


We consider an issue is complete after contributors provided feedback to the templates team and merged a Markdown file of the example document to the _Chronologue Documentation_. We also ask you to add a Markdown file, containing the link to your [Chronologue example document](https://docs-chronologue.netlify.app/), to the folder of the template you tested and name it {templatename-example}.md.

<br>

![Template example](template-example.png "Example of a template folder where a Chronologuer has added a templatename-example.md file that contains a link to the example documentation published in the [Chronologue documentation website.")


<br>

As one of the main goals for creating Chronologue examples is to test the quality of The Good Docs Project templates, we encourage members to use a [friction log](https://docs.google.com/document/d/1f98E-5W8mLVD9jeWz_vjTEOg8oxBrIMuUnaKHxlK0Gk/edit) which can be used to give feedback to the templates team.

To provide feedback to the templates team, create an issue in their [repository.](https://gitlab.com/tgdp/templates/-/issues) You can use your friction log to remember the points of friction or upload the file directly to the ticket. Either way, ensure your feedback is constructive and actionable.

If you want to work on the changes, assign the issue to yourself, and submit a merge request when your work is ready for review by the templates team leads. Check out the [Template Contributing Guide](https://gitlab.com/tgdp/templates/-/blob/main/CONTRIBUTING.md) to ensure you follow their guidelines, tag a [Templates lead](https://thegooddocsproject.dev/who-we-are/#working-group-leads), and merge your changes once they're approved.


## Draft a document

Our contributors come from different backgrounds and have different levels of experience with Git and GitLab. From complete beginners to expert Git users, we want to include everyone in the community review stage. For this reason, we ask our participants to draft their documents in the Google Doc link that is attached to each GitLab ticket. Share the Google Doc link with commenter permission with the Chronologue team during the review process.

You can create a merge request after your document has gone through at least one revision round by the Chronologue team and your working group lead has approved your project to move to that stage.

### Best practices

This is a list of best practices we strive for in the Chronologue project and resources we recommend:


1. Markdown syntax: you can use this [cheat sheet](https://www.markdownguide.org/cheat-sheet/)  to verify the correct use of Markdown syntax.
2. Naming conventions: use lowercase letters and underscores, instead of spaces, to name files.
3. Commit messages: we recommend you read [this guide](https://www.freecodecamp.org/news/how-to-write-better-git-commit-messages/) on how to write good commit messages in Git.
4. Style guide: we follow [Google developer documentation style guide](https://developers.google.com/style).


## Create a merge request

The main branch of the Documentation repository is where we store the files that are published on our documentation site. All merge requests have to be approved by the working group leads before they go live. Submit your merge request to the `/content/en/docs `folder, tag the current leads and notify them on Slack.

Use our naming conventions described in the [Best practices](#best-practices) section to name your Markdown file. You can use an [add-on](https://workspace.google.com/marketplace/app/docs_to_markdown/700168918607) to convert your Google Doc into a Markdown file and use the cheat sheet provided in the [Best practices](#best-practices) section to verify its syntax.

If you’re new to GitLab, you can attend a Git training. Ask the community managers in the #ask-a-community-manager channel about our next sessions.

After a Chronologue lead approves your merge request, your contribution to the Chronologue project will be complete!
