The main goal of the Chronologue project is to write examples of good documentation by testing templates created by the templates team. Use this issue to create a Chronologue example document for the {template name} template.
Link to template in the repository: [Template name](https://gitlab.com/tgdp/templates)

## Link to Google Doc drafts

To keep ownership and manage an archive of the drafts, The Good Docs Project has pre-generated Google doc files for you to test templates and document your process.

- [ ] **Chronologue example document** - [{Content type} example](link-to-Google-Doc)
- [ ] **Decision log** - [{Content type} decisions](link-to-Google-Doc)
- [ ] **Friction log** - [{Content type} friction](link-to-Google-Doc)

Bookmark these links and use them during your writing process. You can always consult the links in this issue.


To know more about the drafting process, please read the [Chronologue Contributing Guide](https://gitlab.com/tgdp/chronologue/docs/-/blob/main/CONTRIBUTING.md?ref_type=heads#overview-of-the-chronologue-documentation-process).

## Definition of done

We consider a document finished when it has gone through all the writing stages and it has (at least) a Chronologue example document and a [link to it in the {template name} repository](https://gitlab.com/tgdp/chronologue/docs/-/blob/main/CONTRIBUTING.md?ref_type=heads#deliverables).

### Writing stages

- [ ] **Proposal stage**. Since the Chronologue tool and world is fake, you need to come up with a possible documentation scenario that can be used to create an example of your template in action. Use the _Decision log_ (Google doc) attached to this to create a proposal for your documentation scenario. Submit your proposal to the Chronologue canon editorial team for feedback and review. They will make sure the documentation scenario makes sense for the Chronologue world and that it doesn't impact existing Chronologue projects. After getting their approval, you can move onto the next phase.
- [ ] **Drafting stage**. Test your template and draft your document in the _Chronologue example document_. During this stage you can also document the friction points you find when using the template in the _Friction log_ file.
- [ ] **Community review stage**. Communicate to your working group leaders when your draft is ready for review by the team. They will schedule a session where team members will read and provide feedback to your document.
- [ ] **Editorial review stage**. When your draft is in a state where you feel it is ready to get merged in, you can work with your working group lead to request an editorial team review. The Chronologue canon editorial team is composed of experienced members of the project who reviews your Chronologue example to ensure that it meets our quality requirements. After getting their approval, you can move onto the next phase.
- [ ] **Merge request stage**. After the team has reviewed your draft at least one time, you can convert your Google doc into a Markdown file and open a merge request in the [Documentation repository](https://gitlab.com/tgdp/chronologue/docs/-/tree/main/content/en/docs?ref_type=heads). Chronologue leads may ask for additional changes. After the group leads approve your merge request, you can merge it.
- [ ] **Template feedback** (optional). If you have any feedback to improve the template you tested, open an issue directly in the templates repository. You can upload the _Friction log_ file. Make sure to give actionable feedback in the issue.


### Chronologue deliverables
To be considered complete, a Chronologue project must have the following files:

- [ ] **Decision log file** - Use this file to propose a possible documentation scenario that can be used to create an example of your template in action. You'll use the decision log to write a proposal for your scenario and then send it to the Chronologue canon editorial team to get feedback on your proposal. They will make sure the documentation scenario makes sense for the Chronologue world and that it doesn't impact existing Chronologue projects. After getting their approval, you can move onto the next stage.
- [ ] **Chronologue example document** - Contains the draft your example document. This file eventually gets merged into the final documentation repository.
- [ ] **Friction log file** - Record any usability problems you had while using the template and indicate possible suggestions for fixes. If needed, use this file at the end of the process to open an issue in templates repository to provide feedback.
- [ ] Link to Chronologue example in the [template repo](https://gitlab.com/tgdp/templates) for that specific template.


## Want to work on this issue?

Great! Make sure you follow our contributing guidelines:

1. Check that the issue is unassigned. If it is assigned, you might be able to work with the current assignee as a paired writer.
2. Join The Good Docs Project by attending a [Welcome Wagon meeting](https://thegooddocsproject.dev/welcome/). You will get access to The Good Docs Project Slack workspace after scheduling or attending this meeting.
3. Read the [Chronologue Contributing Guide](https://gitlab.com/tgdp/chronologue/docs/-/blob/main/CONTRIBUTING.md?ref_type=heads#overview-of-the-chronologue-documentation-process).
4. You are strongly encouraged to join one of the [working groups](https://thegooddocsproject.dev/working-groups/) to get valuable support from the community such as mentorship, Git training, and helpful feedback as you contribute to your first template.
5. Request access to the `chronologue-docs` repository by joining the #tech-requests channel in Slack and posting a request.
6. [Assign yourself to an issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#assignee) for the template that you want to work on.
7. Add the `Chronologue phase:: Proposal` label to the issue.
8. Attend your template working group regularly to receive support and resources for your project.