---
title: API reference
draft: false
menu:
  docs:
    parent: "API"
    identifier: "API Reference"
weight: 30
---
# Chronologue API Reference

## Overview

**What's Chronologue?**

Throughout history, the idea of time travel has always captivated and intrigued people. Researchers
have tried to look back at the past or take a sneak peek into the future
to understand how things worked or would work. From kids to adults to
seniors, many people are curious to know the past or the future. This is
where Chronologue comes into the picture. Now let’s talk about what
Chronologue does.

The Chronologue API is an open-source interface that provides data about astronomical events that have been captured by the Chronologue telescope.
The Chronologue telescope can look into the past and also make event
predictions for the future. 

Users can watch pre-rendered events on
a website, or submit requests for new events to be recorded. The
Chronologue API is an open-source interface that provides data about
astronomical events. You can effortlessly query astronomical data stored in
the API and visualize events on the Chronologue website.

The Chronologue API follows a REST architecture. The website is built
with Next.js. You can access the API's resources via HTTP requests, and
responses are given in JSON format.

### Base URL

The following is the base address for the Chronologue API:

```bash
http://chronologue.netlify.app/api

```

### Version

The current API version is 1.0.

## Events

The methods combined in the Events resource return astronomical events
from the past, the present, or the future. The information can include
an image, a video record, or an article about the event.

### Retrieve all available data 

Returns all past, present, and future events from all locations.

#### Endpoint 

```bash
GET /api/all/
```

#### Request example

```bash
  curl -X GET 'http://chronologue.netlify.app/api/all/'
```

#### Response example

```json
[
  {
    "event": "The Orbiting Astronomical Observatory 2 was the first space telescope  launched",
    "date": "1968-12-07",
    "image": "http://www.sal.wisc.edu/OAO/oao1reg.jpg",
    "video": "",
    "links": [
      "https://en.wikipedia.org/wiki/Space_exploration#Telescope",
      "http://www.sal.wisc.edu/OAO/"
    ],
    "continent": "north-america"
  },
  {
    "event": "launched the first teleportation as a service",
    "date": "3320-2-10",
    "image": "",
    "video": "",
    "links": [
      ""
    ],
    "continent": "asia"
  }
]
```

### Retrieve all events from all locations in a year

Get all the events from all locations that occurred during a specific
year.

#### Endpoint 

```bash
GET /year/{year-number}
```

#### Path parameters

| **Path parameter** | **Type** | **Required** | **Description**                             |
|--------------------|----------|--------------|---------------------------------------------|
| year-number        | int      | required     | The specific year to return all events for. |

#### Request example

```bash
curl -X GET "https://chronologue.netlify.app/api/year/1968"
```

#### Response example

```json
[
    {
        "continent": "north-america",
        "country": "usa",
        "events": [
            {
                "event": "The Orbiting Astronomical Observatory 2 was the first space telescope launched",
                "date": "1968-12-07",
                "image": "http://www.sal.wisc.edu/OAO/oao1reg.jpg",
                "video": "",
                "links": [
                    "https://en.wikipedia.org/wiki/Space_exploration#Telescope",
                    "http://www.sal.wisc.edu/OAO/"
                ]
            }
        ]
    }
]
```

### Retrieve all events in a continent

This API endpoint fetches all the events that occurred in the specified
continent.

#### Endpoint 

```bash
GET /location/{continent-name}
```

#### Path parameters

| **Path parameter** | **Type** | **Required** | **Description**                                                                                         |
|--------------------|----------|--------------|---------------------------------------------------------------------------------------------------------|
| continent-name     | string   | required     | This field is input as the name of the continent. It retrieves the events that occurred on a continent. |

#### Request example

```bash
curl -X GET "https://chronologue.netlify.app/api/location/north-america"
```

#### Response example

```json
[
{
"eventName": "The Orbiting Astronomical Observatory 2 was the first space telescope launched",
"dateName": "1968-12-07T00:00:00.000Z",
"imageName": "http://www.sal.wisc.edu/OAO/oao1reg.jpg",
"videoName": "",
"linkName": [
"https://en.wikipedia.org/wiki/Space_exploration#Telescope",
"http://www.sal.wisc.edu/OAO/"
]
},
{
"eventName": "Voyager 1 became the first human-made object to leave the Solar System into interstellar space",
"dateName": "2012-08-25T00:00:00.000Z",
"imageName": "https://upload.wikimedia.org/wikipedia/commons/6/60/Voyager_spacecraft_model.png",
"videoName": "link",
"linkName": [
"https://en.wikipedia.org/wiki/Space_exploration#Telescope",
"https://en.wikipedia.org/wiki/Voyager_1"
]
},
{
"eventName": "NASA Perseverance rover landed on Mars to find clues on past microbial life",
"dateName": "2021-02-18T00:00:00.000Z",
"imageName": "https://www.nasa.gov/sites/default/files/thumbnails/image/09_touchdown_10k.jpg",
"videoName": "link",
"linkName": [
"https://www.nasa.gov/press-release/nasa-invites-public-to-share-thrill-of-mars-perseverance-rover-landing",
""
]
},
{
"eventName": "NASA Artemis mission sending the first woman and man to the Moon by 2024",
"dateName": "2024-06-07T00:00:00.000Z",
"imageName": "https://blogs.nasa.gov/artemis/wp-content/uploads/sites/303/2021/10/OrionStacked.jpg",
"videoName": "link",
"linkName": [
"https://www.nasa.gov/specials/artemis/",
"https://blogs.nasa.gov/artemis/2021/10/20/stacking-operations-for-artemis-i-mission-nearing-completion/"
]
},
{
"eventName": "Light pollution in Europe is so bad, that you can not see stars with the naked eye any more.",
"dateName": "3000-02-08T00:00:00.000Z",
"imageName": "https://www.darksky.org/wp-content/uploads/2014/09/Light_Pollution_Diagram_680px.jpg",
"videoName": "https://www.youtube.com/watch?v=isoJ0XIKDlc",
"linkName": [
"https://www.darksky.org/light-pollution/",
"https://blue-marble.de/nightlights/2012"
]
}
]
```

### Retrieve events for the specific year and location

Returns events that took place in the given location and year.

#### Endpoint 

```bash
GET /year/{year-number}/location/{continent-name}
```

#### Path parameters

| **Path parameter** | **Type** | **Required** | **Description**                                     |
|--------------------|----------|--------------|-----------------------------------------------------|
| year-number        | int      | required     | The unique year to return all events for.           |
| continent-name     | string   | required     | The unique continent name to return all events for. |

#### Request example

```bash
curl -X GET "https://chronologue.netlify.app/api/year/3320/location/asia"
```

#### Response example

```json
[
    {
        "event": "launched the first teleportation as a service",
        "date": "3320-2-10",
        "image": "",
        "video": "",
        "links": [
            ""
        ]
    }
]
```

## Locations

The methods combined in the Locations resource return locations where
Chronologue has registered some events.

### Retrieve all locations

Returns arrays of where events took place.

#### Endpoint

```bash
GET /location/{location-id}
```

#### Path parameter

| **Path parameter** | **Type** | **Required** | **Description**                              |
|--------------------|----------|--------------|----------------------------------------------|
| location-id        | string   | required     | The unique location ID to return events for. |

#### Request example

```bash
curl --location --request GET 'https://chronologue.netlify.app/api/location/europe'
```

#### Response example

```json
[
    {
        "eventName": "The first successful orbital launch of the Soviet uncrewed Sputnik 1 mission",
        "dateName": "1957-10-04T00:00:00.000Z",
        "imageName": "https://upload.wikimedia.org/wikipedia/commons/b/be/Sputnik_asm.jpg",
        "videoName": "link",
        "linkName": [
            "https://en.wikipedia.org/wiki/Space_exploration#Telescope",
            "https://en.wikipedia.org/wiki/Sputnik_1"
        ]
    },
    {
        "eventName": "The first successful human spaceflight was Vostok 1, carrying the 27-year-old Russian cosmonaut, Yuri Gagarin",
        "dateName": "1961-04-12T00:00:00.000Z",
        "imageName": "https://upload.wikimedia.org/wikipedia/commons/d/df/Vostok_spacecraft.jpg",
        "videoName": "link",
        "linkName": [
            "https://en.wikipedia.org/wiki/Space_exploration#Telescope",
            "https://en.wikipedia.org/wiki/Vostok_1"
        ]
    }
]
```

## Years

The methods combined in the Years resource return years when Chronologue
has registered some events.

### Retrieve a list of years

Retrieves a list of available years to query for data.

#### Endpoint 

```bash
GET /api/all-years-id
```

#### Request example

```bash
curl -X GET 'http://chronologue.netlify.app/api/all-years-id'
```

#### Response example

```json
[
   "1968",
   "1957",
   "1961",
   "2012",
   "2016",
   "2021",
   "2024",
   "3000",
   "2030",
   "3320"
]
```

## Response schema

| **Name**   | **Type**         | **Description**                                                                                                                |
|------------|------------------|--------------------------------------------------------------------------------------------------------------------------------|
| continents | string           | The continent where the event occurred.                                                                                        |
| country    | string           | The country where the event occurred.                                                                                          |
| events     | array            | Contains a list of events that occurred during a year. Each event is an object containing additional information.              |
| eventname  | string           | The event is part of the response and is returned as a string containing the details of the event.                             |
| datename   | DATE(yyyy-mm-dd) | The date on which the event occurred.                                                                                          |
| imagename  | string           | A link to an event related image                                                                                               |
| videoname  | string           | The video is returned as a link which when clicked on displays the video clip related to the event.                            |
| linkname   | string - link    | Links are returned as a response and when clicked on the links, displays the source from which the events have been retrieved. |
