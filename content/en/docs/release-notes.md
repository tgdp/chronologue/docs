# Release notes - Chronologue 2.0.0
## 2023-06-08

As the Chronologue gets up and running, we are consistently finding areas of opportunity for growth (thanks in no small part to users like you). This release cycle, our main priority has been getting our API running. But what good is an API if no one knows how to use it? Therefore, we’ve also leaned on our UX team to create interfaces that make sense.

### New features

#### API requests
You can now make API requests for specific astronomical events that have occurred or will occur. You can make requests for specific events, years, and locations! The API provides responses in JSON format.

### Improvements

#### New request for recording procedure
We’ve updated the process for making requests to be more thorough. The application process is streamlined to give us a better picture of your own research interests and how you will use our website. See [Request for new recording](https://docs-chronologue.netlify.app/docs/t_requesting_a_new_recording/) for more information. 


#### Some design features lost
We’ve migrated our operations to Hugo to make our services more accessible and we prioritized functionality with this migration. While the site is certainly more user-friendly, the new platform is not totally compatible with our previous web design. We're working on improvements and we promise a beautiful UX is coming soon.
