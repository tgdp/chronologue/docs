---
title: "Editor reference"
description: "Internal page for Chronologue writers."
lead: ""
date: 2022-11-07T08:48:57+00:00
lastmod: 2022-11-09T08:48:57+00:00
draft: true
images: []
menu:
  main:
    identifier: "Editor reference"
weight: 900
margin_notes: true
---

> This page is for contributors to the Chronologue project.

## Working with the Doks theme 

This page contains shorthand information about what need to know when you are writing documentation for Chronologue. The theme uses shortcodes  {{< marginal text="Shortcodes are commands for HUGO. Shortcodes are often used to build functionality that isn't originally supported by markdown, such as these margin notes." />}} , some of which come from the Doks theme, others are custom created for the group, such as the margin notes. [^1] 

If you are new to the Doks theme, check out their [Tutorial →](https://getdoks.org/docs/tutorial/introduction/)


## Shortcodes from the Doks theme:

```
{{</* alert icon="👉" text="The Quick Start is intended for intermediate to advanced users." /*/>}}
```
renders as:

{{< alert icon="👉" text="The Quick Start is intended for intermediate to advanced users." />}}


### Relative references: 

TODO: Explanation

```
{{</* relref "quick-start" */>}}
```

### Zippy

{{< details "What is a zippy" open >}}
A zippy is useful for displaying more information.
{{< /details >}}

```
{{</* details "What is a zippy" open */>}}
A zippy is useful for displaying more information.
{{</* details */>}}
```

## Custom shortcodes

### Recipes

Get instructions {{< marginal text="This is a little side note." />}} on how to accomplish common tasks with Doks. [Recipes →](https://getdoks.org/docs/recipes/project-configuration/)

### Reference Guides

Learn how to customize Doks to fully make it your own. [Reference Guides →](https://getdoks.org/docs/reference-guides/security/)

### Extensions

Get instructions on {{< marginal >}}
This is another side note. This note is pretty long and should wrap up to the right of where it is found in the document. This note is pretty long and should wrap up to the right of where it is found in the document. This note is pretty long and should wrap up to the right of where it is found in the document. This note is pretty long and should wrap up to the right of where it is found in the document. This note is pretty long and should wrap up to the right of where it is found in the document. This note is pretty long and should wrap up to the right of where it is found in the document. This note is pretty long and should wrap up to the right of where it is found in the document. This note is pretty long and should wrap up to the right of where it is found in the document.
{{< /marginal >}} how to add even more to Doks. [Extensions →](https://getdoks.org/docs/extensions/breadcrumb-navigation/)

[^1]: And that's the footnote.