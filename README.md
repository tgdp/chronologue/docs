# Chronologue Project documentation

by [The Good Docs Project](https://gitlab.com/tgdp/chronologue/mock-tool/-/tree/main)

[![License: MIT-0](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/license/mit-0) [![CHAOSS DEI: Bronze](https://gitlab.com/tgdp/governance/-/raw/main/CHAOSS-Bronze-Badge-small.png?ref_type=heads)](https://badging.chaoss.community/)

The Chronologue Project is a fictional software project that creates examples of good documentation. Members of the Chronologue working group use templates to write high-quality documentation. If you're a new member of the Chronologue working group, you might find that the concept of building real documentation for a fictional project is confusing. Familiarizing yourself with the group’s documentation, objectives, and processes can help you get oriented.

This is a repository for Chronologue Project documentation. There is a peer repository for the Chronologue tool at [Chronologue Mock Tool](https://gitlab.com/tgdp/chronologue/mock-tool). (To learn about the Mock Tool, see the README in the Mock Tool repository.)

## Documentation site

The documentation that the Chronologue working group creates is on [The Chronologue](https://docs-chronologue.netlify.app/) site.

## Objectives

The Chronologue working group contributes to [The Good Docs Project](https://gitlab.com/tgdp/chronologue/mock-tool/-/tree/main) by:

1. Creating examples of high-quality documentation: The Chronologue working group uses templates created by the templates working group to build documentation. This documentation serves as a model for people learning to use the templates.
1. Providing quality assurance for templates: The Chronologue working group ensures that Chronologue Project [templates](https://gitlab.com/tgdp/templates) are easy to use by testing the templates and suggesting improvements where needed.
1. Showcasing great docs in action: The Chronologue working group teaches people what great documentation looks like by creating a high-quality docs site.

## Process
This is a high-level overview of the process. For more detail, see the [Contributing Guide](https://gitlab.com/tgdp/chronologue/docs/-/blob/main/CONTRIBUTING.md?ref_type=heads).

1. When a documentation template is added to the templates repository, the templates working group leads notify the Chronologue working group leads that the template was featured in a template release.
1. In the next release cycle, the Chronologue working group uses the template to build documentation for the Chronologue tool. (To learn what the Chronologue tool is, see [How Chronologue works](https://docs-chronologue.netlify.app/docs/c_how_chronologue_works/))
1. The documentation is added to the Chronologue repository.

## Contributing guide

- [Contributing Guide](--ADD LINK TO CONTRIBUTING GUIDE--)

## Additional documentation

- [Chronologue working group](https://thegooddocsproject.dev/working-group/chronologue/)
- [How Chronologue works](https://docs-chronologue.netlify.app/docs/c_how_chronologue_works/)
- [Chronologue Mock Tool repository](https://gitlab.com/tgdp/chronologue/mock-tool)

## How to get help

Here’s how to get help, share ideas, or ask questions:

- Post to the #chronologue-docs Slack channel at [The Good Docs](https://thegooddocs.slack.com/archives/C016L3962CU). NOTE: If you’re not a member of The Good Docs Slack workspace, fill out the [Welcome Wagon form](https://thegooddocsproject.dev/welcome/) to attend a Welcome Wagon meeting. After you attend the meeting, you’ll receive an invitation to the workspace.
- Attend a weekly meeting of the [Chronologue working group](https://thegooddocsproject.dev/working-group/chronologue/).


## License and terms of use

This project is licensed under the [BSD Zero Clause License](https://gitlab.com/tgdp/chronologue/docs/-/blob/main/LICENSE).
